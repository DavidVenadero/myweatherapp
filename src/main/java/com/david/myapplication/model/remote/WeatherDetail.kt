package com.david.myapplication.model.remote

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WeatherDetail(
    val description: String,
    val icon: String,
    val id: Int,
    val main: String
): Parcelable