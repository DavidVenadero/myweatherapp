package com.david.myapplication.repository

import com.david.myapplication.model.remote.WeatherResponse
import retrofit2.Response

interface WeatherRepository {
    suspend fun getWeather(city:String):Response<WeatherResponse>
}