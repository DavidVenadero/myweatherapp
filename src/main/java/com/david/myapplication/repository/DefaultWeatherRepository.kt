package com.david.myapplication.repository

import com.david.myapplication.datasource.WeatherRemote
import com.david.myapplication.model.remote.WeatherResponse
import retrofit2.Response
import javax.inject.Inject

class DefaultWeatherRepository @Inject constructor(
    private val weatherRemote: WeatherRemote
) :WeatherRepository {
    override suspend fun getWeather(city: String): Response<WeatherResponse> =
        weatherRemote.getWeather(city)
}