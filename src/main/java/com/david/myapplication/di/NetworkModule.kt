package com.david.myapplication.di

import com.david.myapplication.datasource.DefaultWeatherRemote
import com.david.myapplication.datasource.WeatherRemote
import com.david.myapplication.datasource.WeatherService
import com.david.myapplication.repository.DefaultWeatherRepository
import com.david.myapplication.repository.WeatherRepository
import com.david.myapplication.utils.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): WeatherService =
        retrofit.create(WeatherService::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(service: WeatherService): WeatherRemote =
        DefaultWeatherRemote(service)

    @Singleton
    @Provides
    fun provideRepository(weatherRemote: WeatherRemote):WeatherRepository=
        DefaultWeatherRepository(weatherRemote)
}