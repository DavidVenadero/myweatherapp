package com.david.myapplication.datasource

import com.david.myapplication.model.remote.WeatherResponse
import retrofit2.Response
import javax.inject.Inject

class DefaultWeatherRemote @Inject constructor(
   private val weatherService: WeatherService
):WeatherRemote {
    override suspend fun getWeather(city: String): Response<WeatherResponse> =
        weatherService.getWeather(city)
}