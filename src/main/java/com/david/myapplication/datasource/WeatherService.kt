package com.david.myapplication.datasource

import com.david.myapplication.model.remote.WeatherResponse
import com.david.myapplication.utils.Constants.WEATHER_API_ID
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherService {
    @GET("forecast")
    suspend fun getWeather(@Query("q") city: String,
                           @Query("appid") app_id:String = WEATHER_API_ID):Response<WeatherResponse>
}