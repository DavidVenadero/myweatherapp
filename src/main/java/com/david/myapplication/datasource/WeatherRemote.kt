package com.david.myapplication.datasource

import com.david.myapplication.model.remote.WeatherResponse
import retrofit2.Response

interface WeatherRemote {
    suspend fun getWeather(city:String): Response<WeatherResponse>
}