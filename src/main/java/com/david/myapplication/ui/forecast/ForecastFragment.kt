package com.david.myapplication.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.david.myapplication.R
import com.david.myapplication.model.remote.WeatherResponse
import com.david.myapplication.ui.adapter.WeatherAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_forecast.*

@AndroidEntryPoint
class ForecastFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_forecast, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<WeatherResponse>("weatherResponse")?.let {
            setupUi(it)
        }
    }

    private fun setupUi(weatherResponse: WeatherResponse) {
        rv_weathers.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        val weatherAdapter = WeatherAdapter()
        weatherAdapter.setItems(weatherResponse.list)
        weatherAdapter.setOnClick {
            findNavController().navigate(R.id.action_forecastFragment_to_forecastDetailFragment, bundleOf("weather" to it))
        }
        rv_weathers.adapter = weatherAdapter
    }
}