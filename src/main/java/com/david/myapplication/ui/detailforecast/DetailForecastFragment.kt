package com.david.myapplication.ui.detailforecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.david.myapplication.R
import com.david.myapplication.model.remote.Weather
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_detail_forecast.*

@AndroidEntryPoint
class DetailForecastFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_detail_forecast, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.getParcelable<Weather>("weather")?.let {
            setupUi(it)
        }
    }

    private fun setupUi(weather: Weather) {
        tv_wind_force.text = weather.clouds.all.toString()
        tv_feels_like.text = getString(R.string.feels_like, weather.main.feels_like)
        tv_title.text = weather.weather[0].main
        tv_description.text = weather.weather[0].description
    }
}