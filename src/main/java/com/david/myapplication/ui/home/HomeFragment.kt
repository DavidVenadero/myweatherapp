package com.david.myapplication.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.david.myapplication.R
import com.david.myapplication.ui.BaseFragment
import com.david.myapplication.ui.viewmodel.CommonViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*

@AndroidEntryPoint
class HomeFragment : Fragment(), BaseFragment {
    private val commonViewModel: CommonViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onStart() {
        super.onStart()
        startElements()
        startObservers()
    }

    override fun startObservers() {
        commonViewModel.weatherResponse.observe(viewLifecycleOwner) {
            val city = requireActivity().findViewById<TextView>(R.id.tv_cityName)
            city.text = edt_city.text.toString()
            findNavController().navigate(R.id.forecastFragment, bundleOf("weatherResponse" to it))
        }
    }

    override fun startElements() {
        btn_look_up_city.setOnClickListener {
            commonViewModel.getWeather(edt_city.text.toString())
        }
    }

}