package com.david.myapplication.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.david.myapplication.model.remote.WeatherResponse
import com.david.myapplication.repository.WeatherRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CommonViewModel @Inject constructor(
    private val weatherRepository: WeatherRepository
) : ViewModel() {
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
    private val _weatherResponse = MutableLiveData<WeatherResponse>()
    val weatherResponse: LiveData<WeatherResponse>
        get() = _weatherResponse

    fun getWeather(city: String) {
        viewModelScope.launch(dispatcher) {
            val response = weatherRepository.getWeather(city)
            if (response.isSuccessful) {
                _weatherResponse.postValue(response.body())
            }
        }

    }
}