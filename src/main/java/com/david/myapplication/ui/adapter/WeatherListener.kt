package com.david.myapplication.ui.adapter

import com.david.myapplication.model.remote.Weather

interface WeatherListener {
     var listener: (weather: Weather) -> Unit
}