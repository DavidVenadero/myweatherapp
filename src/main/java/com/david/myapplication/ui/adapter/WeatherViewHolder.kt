package com.david.myapplication.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.david.myapplication.R
import com.david.myapplication.databinding.ElementWeatherBinding
import com.david.myapplication.model.remote.Weather

class WeatherViewHolder(
    private val item: ElementWeatherBinding,
    var listener: (weather: Weather) -> Unit
) : RecyclerView.ViewHolder(item.root) {
    fun bind(weather: Weather) {
        item.tvName.text = weather.weather[0].main
        item.tvDetail.text = item.tvDetail.context.getString(R.string.temp, weather.main.temp)
        item.root.setOnClickListener {
            listener(weather)
        }
    }
}