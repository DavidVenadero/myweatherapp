package com.david.myapplication.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.david.myapplication.databinding.ElementWeatherBinding
import com.david.myapplication.model.remote.Weather

class WeatherAdapter() : RecyclerView.Adapter<WeatherViewHolder>(), WeatherListener {
    override lateinit var listener: (weather: Weather) -> Unit
    private val items = ArrayList<Weather>()
    fun setItems(items: List<Weather>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherViewHolder {
        val binding: ElementWeatherBinding =
            ElementWeatherBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        return WeatherViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: WeatherViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size

    fun setOnClick(next: (weather: Weather) -> Unit) {
        listener = next
    }
}