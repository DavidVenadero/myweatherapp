package com.david.myapplication.ui

interface BaseFragment {
    fun startElements()
    fun startObservers()
}